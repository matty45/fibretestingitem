package sticky;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sticky.items.Stick;

public class TestingStick implements ModInitializer {

    public static Logger LOGGER = LogManager.getLogger();

    public static final String MOD_ID = "testingstick";
    public static final String MOD_NAME = "TestingStick";

    // Add the amazing testing stick
    public static final Stick TESTING_STICK = new Stick(new Item.Settings().group(ItemGroup.MISC).maxDamage(100));

    @Override
    public void onInitialize() {
        log(Level.INFO, "Initializing");
        Registry.register(Registry.ITEM, new Identifier(TestingStick.MOD_ID, "test_stick"), TESTING_STICK);
        log(Level.INFO, "Initialized");
    }

    private static void log(Level level, String message){
        LOGGER.log(level, "["+MOD_NAME+"] " + message);
    }

}
