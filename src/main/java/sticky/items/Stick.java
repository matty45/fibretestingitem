package sticky.items;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class Stick extends Item
{
    public Stick(Settings settings)
    {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity Player, Hand hand)
    {
        ItemStack ItemInHand = Player.getStackInHand(hand);
        // TODO: Do whatever here
        return new TypedActionResult<>(ActionResult.SUCCESS, ItemInHand);
    }
}